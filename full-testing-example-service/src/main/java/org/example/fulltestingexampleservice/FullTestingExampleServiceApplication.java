package org.example.fulltestingexampleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullTestingExampleServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FullTestingExampleServiceApplication.class, args);
    }

}
