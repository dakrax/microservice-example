package org.example.fulltestingexampleservice.register;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class RegisterEventListener {

    private final RegisterService registerService;


    @KafkaListener(topics = "${register.topic.name}", groupId = "example")
    void onRegister(RegisterEvent registerEvent){
        log.info("Incoming register event {}",registerEvent);
        registerService.register(registerEvent);
    }

}
