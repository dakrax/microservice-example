package org.example.fulltestingexampleservice.register;

import lombok.Data;

@Data
public class RegisterEvent {
    private String userId;
    private String login;
    private String password;
    private String system;
}
