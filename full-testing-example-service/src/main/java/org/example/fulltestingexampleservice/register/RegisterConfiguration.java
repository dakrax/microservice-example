package org.example.fulltestingexampleservice.register;

import org.example.fulltestingexampleservice.esystem.ESystemService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
public class RegisterConfiguration {
    @Bean
    RegisterPersistanceOperation registerPersistanceOperation(NamedParameterJdbcTemplate jdbcTemplate) {
        return new RegisterPersistanceOperation(jdbcTemplate);
    }

    @Bean
    RegisterService registerService(
            RegisterPersistanceOperation persistanceOperation,
            List<RegisterHandler> registerHandlerList
    ) {
        Map<String, RegisterHandler> handlerMap = registerHandlerList.stream()
                .collect(Collectors.toMap(RegisterHandler::systemName, handler -> handler));

        return new RegisterService(persistanceOperation, handlerMap);
    }
}
