package org.example.fulltestingexampleservice.register;

public interface RegisterHandler {
    void register(String login, String password);
    String systemName();
}
