package org.example.fulltestingexampleservice.esystem;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Slf4j
public class ESystemClient {
    private final RestTemplate restTemplate;
    private final String baseUri;

    public ESystemRegisterResponse register(ESystemRegisterRequest request) {
        RequestEntity<ESystemRegisterRequest> requestEntity = RequestEntity.post(
                        baseUri + "/register"
                )
                .body(request);

        try {
            ResponseEntity<ESystemRegisterResponse> response = restTemplate.exchange(
                    requestEntity,
                    ESystemRegisterResponse.class
            );

            return response.getBody();
        } catch (HttpClientErrorException e) {
            log.warn("Invalid response from ESystem for [{}]: {}", request, e.getResponseBodyAsString());
            throw new IllegalArgumentException("Invalid response from ESystem for user registration " + e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.warn("ESystem unavailable [{}]", request);
            throw new IllegalStateException("ESystem unavailable");
        }
    }
}
