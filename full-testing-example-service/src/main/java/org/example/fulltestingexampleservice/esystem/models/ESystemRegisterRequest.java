package org.example.fulltestingexampleservice.esystem.models;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class ESystemRegisterRequest {
    String login;
    String password;
}
