package org.example.fulltestingexampleservice.maxsystem;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterResponse;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Slf4j
public class MaxSystemClient {
    private final RestTemplate restTemplate;
    private final String baseUri;

    public MaxSystemRegisterResponse register(MaxSystemRegisterRequest request) {
        RequestEntity<MaxSystemRegisterRequest> requestEntity = RequestEntity.post(
                        baseUri + "/register"
                )
                .body(request);

        try {
            ResponseEntity<MaxSystemRegisterResponse> response = restTemplate.exchange(
                    requestEntity,
                    MaxSystemRegisterResponse.class
            );

            return response.getBody();
        } catch (HttpClientErrorException e) {
            log.warn("Invalid response from ESystem for [{}]: {}", request, e.getResponseBodyAsString());
            throw new IllegalArgumentException("Invalid response from ESystem for user registration " + e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.warn("ESystem unavailable [{}]", request);
            throw new IllegalStateException("ESystem unavailable");
        }
    }
}
