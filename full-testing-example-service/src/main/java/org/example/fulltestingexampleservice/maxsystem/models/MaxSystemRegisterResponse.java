package org.example.fulltestingexampleservice.maxsystem.models;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class MaxSystemRegisterResponse {
    Result result;
    Error error;
    public enum Result {
        OK,FAIL
    }
}

