package org.example.fulltestingexampleservice.maxsystem;

import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.esystem.ESystemClient;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterResponse;
import org.example.fulltestingexampleservice.register.RegisterHandler;

@RequiredArgsConstructor
public class MaxSystemService implements RegisterHandler {
    private final MaxSystemClient maxSystemClient;


    @Override
    public void register(String login, String password) {
        MaxSystemRegisterResponse response = maxSystemClient.register(
                MaxSystemRegisterRequest.builder()
                        .login(login)
                        .password(password)
                        .build()
        );

        if(response.getResult()!= MaxSystemRegisterResponse.Result.OK){
            throw new IllegalArgumentException(response.getError());
        }
    }

    @Override
    public String systemName() {
        return "max-system";
    }
}
