package org.example.fulltestingexampleservice.maxsystem;

import org.example.fulltestingexampleservice.esystem.ESystemClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MaxSystemConfiguration {

    @Bean
    MaxSystemClient maxSystemClient(
            RestTemplate restTemplate,
            @Value("${services.max-system.url}") String url
    ) {
        return new MaxSystemClient(restTemplate, url);
    }
}
