package org.example.fulltestingexampleservice.register;

import org.example.fulltestingexampleservice.common.models.RegisterResult;
import org.example.fulltestingexampleservice.esystem.ESystemService;
import org.example.fulltestingexampleservice.maxsystem.MaxSystemService;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RegisterServiceTest {
    @InjectMocks
    private RegisterService registerService;
    @Mock
    private MaxSystemService maxSystemService;
    @Mock
    private ESystemService eSystemService;
    @Mock
    private RegisterPersistanceOperation registerPersistanceOperation;

    @Mock
    Logger logger;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        registerService = new RegisterService(registerPersistanceOperation, getHashMap());
    }

    @Test
    void getResultIsOk() {
        String login = "max";
        Optional<RegisterResult> optionalResult = Optional.of(new RegisterResult());
        optionalResult.get().setLogin(login);

        when(registerPersistanceOperation.findById(any(String.class))).thenReturn(optionalResult);

        RegisterResult result = registerService.getResult(login);

        assertEquals(result.getLogin(), login);
    }
    @Test
    void getResultWithError() {
        String login = "max";

        when(registerPersistanceOperation.findById(any(String.class))).thenThrow(new IllegalArgumentException());

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            RegisterResult result = registerService.getResult(login);
        });
    }

    @ParameterizedTest()
    @ValueSource(strings = {"e-system", "max-system"})
    void registerIsOk(String system) {
        RegisterEvent registerEvent = getRegisterEvent("max", "1", system, "1");
        RegisterResult registerResult = getRegisterResult(registerEvent.getLogin(), registerEvent.getUserId());

        doNothing().when(maxSystemService).register(Mockito.anyString(), Mockito.anyString());

        registerService.register(registerEvent);


        verify(registerPersistanceOperation, times(1)).save(registerResult);
    }
    @ParameterizedTest()
    @ValueSource(strings = {"1"})
    void registerWrongSystemMessage(String system) {
        RegisterEvent registerEvent = getRegisterEvent("max", "1", system, "1");
        RegisterResult registerResult = getRegisterResult(registerEvent.getLogin(), registerEvent.getUserId());
        registerResult.setStatus("FAIL");
        doNothing().when(maxSystemService).register(Mockito.anyString(), Mockito.anyString());

        registerService.register(registerEvent);


        verify(registerPersistanceOperation, times(1)).save(registerResult);
    }
    @ParameterizedTest()
    @ValueSource(strings = {"e-system", "max-system"})
    void registerThrowsException(String system) {
        RegisterEvent registerEvent = getRegisterEvent("max", "1", system, "1");
        RegisterResult registerResult = getRegisterResult(registerEvent.getLogin(), registerEvent.getUserId());
        registerResult.setStatus("FAIL");
        doThrow(new IllegalArgumentException()).when(eSystemService).register(Mockito.anyString(), Mockito.anyString());
        doThrow(new IllegalArgumentException()).when(maxSystemService).register(Mockito.anyString(), Mockito.anyString());

        registerService.register(registerEvent);


        verify(registerPersistanceOperation, times(1)).save(registerResult);
    }

    private RegisterEvent getRegisterEvent(String login, String password, String system, String userId){
        RegisterEvent registerEvent = new RegisterEvent();
        registerEvent.setLogin(login);
        registerEvent.setPassword(password);
        registerEvent.setSystem(system);
        registerEvent.setUserId(userId);

        return registerEvent;
    }
    private RegisterResult getRegisterResult(String login, String uid){
        RegisterResult registerResult = new RegisterResult();
        registerResult.setLogin(login);
        registerResult.setUserId(uid);
        registerResult.setStatus("OK");

        return registerResult;
    }

    private HashMap<String, RegisterHandler> getHashMap(){
        HashMap<String, RegisterHandler> hashMap = new HashMap<>();

        hashMap.put("max-system", maxSystemService);
        hashMap.put("e-system", eSystemService);

        return hashMap;
    }
}