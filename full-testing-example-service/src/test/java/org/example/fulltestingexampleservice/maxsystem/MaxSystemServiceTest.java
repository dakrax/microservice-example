package org.example.fulltestingexampleservice.maxsystem;

import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterResponse;
import org.example.fulltestingexampleservice.register.RegisterHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class MaxSystemServiceTest {

    private RegisterHandler maxSystemRegisterHandler;
    @Mock
    private MaxSystemClient maxSystemClient;

    @BeforeEach
    public void SetUp(){
        MockitoAnnotations.openMocks(this);
        maxSystemRegisterHandler = new MaxSystemService(maxSystemClient);
    }
    @Test
    void registerShouldReturnError() {
        MaxSystemRegisterResponse maxSystemRegisterResponse = MaxSystemRegisterResponse
                .builder()
                .result(MaxSystemRegisterResponse.Result.FAIL)
                .build();
        when(maxSystemClient.register(any(MaxSystemRegisterRequest.class))).thenReturn(maxSystemRegisterResponse);

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            maxSystemRegisterHandler.register("max", "max");
        });
    }
    @Test
    void registerShouldBeOk() {
        MaxSystemRegisterResponse maxSystemRegisterResponse = MaxSystemRegisterResponse
                .builder()
                .result(MaxSystemRegisterResponse.Result.OK)
                .build();
        when(maxSystemClient.register(any(MaxSystemRegisterRequest.class))).thenReturn(maxSystemRegisterResponse);

        maxSystemRegisterHandler.register("max", "max");
    }

    @Test
    void systemName() {

        String systemName = maxSystemRegisterHandler.systemName();

        assertEquals(systemName, "max-system");
    }
}