package org.example.fulltestingexampleservice.esystem;

import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterRequest;
import org.example.fulltestingexampleservice.esystem.models.ESystemRegisterResponse;
import org.example.fulltestingexampleservice.maxsystem.MaxSystemClient;
import org.example.fulltestingexampleservice.maxsystem.MaxSystemService;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterResponse;
import org.example.fulltestingexampleservice.register.RegisterHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ESystemServiceTest {
    private RegisterHandler eSystemRegisterHandler;
    @Mock
    private ESystemClient eSystemClient;

    @BeforeEach
    public void SetUp(){
        MockitoAnnotations.openMocks(this);
        eSystemRegisterHandler = new ESystemService(eSystemClient);
    }
    @Test
    void registerShouldReturnError() {
        ESystemRegisterResponse eSystemRegisterResponse = ESystemRegisterResponse
                .builder()
                .result(ESystemRegisterResponse.Result.FAIL)
                .build();
        when(eSystemClient.register(any(ESystemRegisterRequest.class))).thenReturn(eSystemRegisterResponse);

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            eSystemRegisterHandler.register("max", "max");
        });
    }
    @Test
    void registerShouldBeOk() {
        ESystemRegisterResponse eSystemRegisterResponse = ESystemRegisterResponse
                .builder()
                .result(ESystemRegisterResponse.Result.OK)
                .build();
        when(eSystemClient.register(any(ESystemRegisterRequest.class))).thenReturn(eSystemRegisterResponse);

        eSystemRegisterHandler.register("max", "max");
    }

    @Test
    void systemName() {

        String systemName = eSystemRegisterHandler.systemName();

        assertEquals(systemName, "e-system");
    }
}