package org.example.fulltestingexampleservicefunctionaltests.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import kafka.Kafka;
import org.example.fulltestingexampleservice.register.RegisterPersistanceOperation;
import org.example.fulltestingexampleservicefunctionaltests.steps.ESystemSteps;
import org.example.fulltestingexampleservicefunctionaltests.steps.JsonSteps;
import org.example.fulltestingexampleservicefunctionaltests.steps.MaxSystemSteps;
import org.example.fulltestingexampleservicefunctionaltests.steps.RegisterSteps;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;

@TestConfiguration
public class StepsConfiguration {


    @Bean
    JsonSteps jsonSteps(ObjectMapper objectMapper) {
        return new JsonSteps(objectMapper);
    }

    @Bean
    ESystemSteps eSystemSteps(JsonSteps jsonSteps) {
        return new ESystemSteps(jsonSteps);
    }

    @Bean
    MaxSystemSteps maxSystemSteps(JsonSteps jsonSteps) {
        return new MaxSystemSteps(jsonSteps);
    }

    @Bean
    RegisterSteps registerSteps(
            RegisterPersistanceOperation registerPersistanceOperation,
            TestRestTemplate restTemplate,
            KafkaTemplate<String,Object> kafkaTemplate,
            @Value("${register.topic.name}") String topic
    ) {
        return new RegisterSteps(registerPersistanceOperation, restTemplate,kafkaTemplate,topic);
    }
}
