package org.example.fulltestingexampleservicefunctionaltests;


import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.example.fulltestingexampleservice.FullTestingExampleServiceApplication;
import org.example.fulltestingexampleservicefunctionaltests.configuration.StepsConfiguration;
import org.example.fulltestingexampleservicefunctionaltests.steps.ESystemSteps;
import org.example.fulltestingexampleservicefunctionaltests.steps.MaxSystemSteps;
import org.example.fulltestingexampleservicefunctionaltests.steps.RegisterSteps;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest(
        classes = {
                FullTestingExampleServiceApplication.class,
                BaseFunctionalTest.FunctionTestConfiguration.class,
                StepsConfiguration.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 1234)
public abstract class BaseFunctionalTest {

    public static final String USER_ID = "user1234";
    public static final String LOGIN = "vasia";
    public static final String PASSWORD = "0000";

    @LocalServerPort
    protected int serverPort;

    @Autowired
    ESystemSteps eSystemSteps;

    @Autowired
    MaxSystemSteps maxSystemSteps;

    @Autowired
    RegisterSteps registerSteps;


    @TestConfiguration
    public static class FunctionTestConfiguration {
        @Autowired
        DataSource dataSource;
    }

}
