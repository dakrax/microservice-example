package org.example.fulltestingexampleservicefunctionaltests;

import org.example.fulltestingexampleservice.common.models.RegisterResult;
import org.example.fulltestingexampleservice.register.RegisterStatusResponse;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

public class MaxSystemFlowTest extends BaseFunctionalTest {
    @Test
    void shouldSuccessRegister() {
        RegisterStatusResponse expected = RegisterStatusResponse.builder()
                .userId(USER_ID)
                .result(RegisterStatusResponse.Result.OK)
                .build();

        maxSystemSteps.stubSuccessRegistration(LOGIN, PASSWORD);

        registerSteps.sendRegisterMessage("max-system",USER_ID,LOGIN,PASSWORD);

        await().atMost(Duration.of(30, ChronoUnit.SECONDS)).untilAsserted(()->{
            Optional<RegisterResult> state = registerSteps.selectState(USER_ID);
            assertThat(state).hasValue(createRegisterResult(USER_ID,LOGIN,"OK"));
        });

        RegisterStatusResponse response = registerSteps.getStatusResponse(USER_ID);
        assertThat(response).isEqualTo(expected);
    }

    private RegisterResult createRegisterResult(String userId, String login, String ok) {
        var res = new RegisterResult();
        res.setUserId(userId);
        res.setStatus(ok);
        res.setLogin(login);
        return res;
    }
}
