package org.example.fulltestingexampleservicefunctionaltests.steps;

import lombok.RequiredArgsConstructor;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterRequest;
import org.example.fulltestingexampleservice.maxsystem.models.MaxSystemRegisterResponse;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;

@RequiredArgsConstructor
public class MaxSystemSteps {
    private final JsonSteps jsonSteps;

    public void stubSuccessRegistration(String login, String password) {
        MaxSystemRegisterResponse response = MaxSystemRegisterResponse.builder()
                .result(MaxSystemRegisterResponse.Result.OK)
                .build();

        MaxSystemRegisterRequest request = MaxSystemRegisterRequest.builder()
                .login(login)
                .password(password)
                .build();

        stubFor(
                post("/register")
                        .withRequestBody(equalToJson(jsonSteps.toJson(request)))
                        .willReturn(okJson(jsonSteps.toJson(response)))
        );
    }
}
